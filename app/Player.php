<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $fillable = [
      'user_id',
      'team_id'
    ];

    public function user(){
      return $this->belongsTo('App\User');
    }

    public function team(){
      return $this->belongsTo('App\Team');
    }
}