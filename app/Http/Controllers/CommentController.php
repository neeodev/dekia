<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Comment;

class CommentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index($article){
        $comments = Comment::where('article_id', $article)->get();

        return $this->jsonResponse($comments);
    }

    public function show($article, $id){
        $comment = Comment::where('article_id', $article)->find($id);

        return $this->jsonResponse($comment);
    }

    public function create($article, Request $request){
        $title = $request->input('title');
        $content = $request->input('content');
        $user_id = 1;

        $comment = Comment::create([
            'title' => $title,
            'content' => $content,
            'user_id' => $user_id,
            'article_id' => $article
        ]);

        return $this->jsonResponse($comment, 201);
    }

    public function update($article, Request $request, $id){
        $comment = Comment::where('article_id', $article)->find($id);

        $title = $request->input('title');
        $content = $request->input('content');

        $comment->update([
            'title' => $title,
            'content' => $content
        ]);

        return $this->jsonResponse($comment);
    }

    public function destroy($id){
        if (Comment::destroy($id)){
            $code = 204;
        }else{
            $code = 403;
        }

        return $this->jsonResponse(null, $code);
    }
}
