<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Article;
use Illuminate\Support\Str;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        $articles = Article::with(['user','tags'])->get();

        return $this->jsonResponse($articles);
    }

    public function show($slug){
        $article = Article::where('slug', $slug)->with(['user','tags'])->get();

        return $this->jsonResponse($article);
    }

    public function create(Request $request){
        $title = $request->input('title');
        $slug = Str::slug($title);
        $content = $request->input('content');
        $user_id = 1;

        $article = Article::create([
            'title' => $title,
            'slug' => $slug,
            'content' => $content,
            'user_id' => $user_id
        ]);

        return $this->jsonResponse($article, 201);
    }

    public function update(Request $request, $id){
        $article = Article::find($id);

        $title = $request->input('title');
        $slug = Str::slug($title);
        $content = $request->input('content');

        $article->update([
            'title' => $title,
            'slug' => $slug,
            'content' => $content
        ]);

        return $this->jsonResponse($article);
    }

    public function destroy($id){
        if (Article::destroy($id)){
            $code = 204;
        }else{
            $code = 403;
        }

        return $this->jsonResponse(null, $code);
    }
}
