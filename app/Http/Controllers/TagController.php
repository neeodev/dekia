<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Tag;

class TagController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        $tags = Tag::with('articles')->get();

        return $this->jsonResponse($tags);
    }

    public function show($id){
        $tag = Tag::with('articles')->find($id);

        return $this->jsonResponse($tag);
    }

    public function create(Request $request){
        $name = $request->input('name');

        $tag = Tag::create([
            'name' => $name
        ]);

        return $this->jsonResponse($tag, 201);
    }

    public function update(Request $request, $id){
        $tag = Tag::find($id);

        $name = $request->input('name');

        $tag->update([
            'name' => $name
        ]);

        return $this->jsonResponse($tag);
    }

    public function destroy($id){
        if (Tag::destroy($id)){
            $code = 204;
        }else{
            $code = 403;
        }

        return $this->jsonResponse(null, $code);
    }
}
