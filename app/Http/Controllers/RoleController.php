<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Role;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        //
    }

    public function index(){
        $roles = Role::with('users')->get();

        return $this->jsonResponse($roles);
    }

    public function show($id){
        $role = Role::with('users')->find($id);

        return $this->jsonResponse($role);
    }

    public function create(Request $request){
        $name = $request->input('name');
        $description = $request->input('description');

        $role = Role::create([
            'name' => $name,
            'description' => $description
        ]);

        return $this->jsonResponse($role, 201);
    }

    public function update(Request $request, $id){
        $role = Role::find($id);

        $name = $request->input('name');
        $description = $request->input('description');

        $role->update([
            'name' => $name,
            'description' => $description
        ]);

        return $this->jsonResponse($role);
    }

    public function destroy($id){
        if (Role::destroy($id)){
            $code = 204;
        }else{
            $code = 403;
        }

        return $this->jsonResponse(null, $code);
    }
}
