<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Team;

class TeamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function all(){
        $teams = Team::with(['user','game'])->get();

        return $this->jsonResponse($teams);
    }

    public function index($game){
        $teams = Team::where('game_id', $game)->with('user')->get();

        return $this->jsonResponse($teams);
    }

    public function show($game, $id){
        $team = Team::where('game_id', $game)->with('user')->find($id);

        return $this->jsonResponse($team);
    }

    public function create($game, Request $request){
        $name = $request->input('name');
        $user_id = 1;

        $team = Team::create([
            'name' => $name,
            'user_id' => $user_id,
            'game_id' => $game
        ]);

        return $this->jsonResponse($team);
    }

    public function update($game, Request $request, $id){
        $team = Team::where('game_id', $game)->with('user')->find($id);

        $name = $request->input('name');

        $team->update([
            'name' => $name
        ]);

        return $this->jsonResponse($team);
    }

    public function destroy($id){
        if (Team::destroy($id)){
            $code = 204;
        }else{
            $code = 403;
        }

        return $this->jsonResponse(null, $code);
    }
}
