<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        $users = User::with('role')->get();

        return $this->jsonResponse($users);
    }

    public function show($id){
        $user = User::with('role')->find($id);

        return $this->jsonResponse($user);
    }

    public function create(Request $request){

        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $pseudo = $request->input('pseudo');
        $email = $request->input('email');
        $password = $request->input('password');
        $token = $this->generateToken();
        $role = 2;

        $user = User::create([
            'firstname' => $firstname,
            'lastname' => $lastname,
            'pseudo' => $pseudo,
            'email' => $email,
            'password' => $password,
            'token' => $token,
            'role_id' => $role
        ]);

        return $this->jsonResponse($user, 201);
    }

    public function update(Request $request, $id){
        $user = User::find($id);

        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $pseudo = $request->input('pseudo');
        $email = $request->input('email');
        $password = $request->input('password');
        $role = $request->input('role');

        $user->update([
            'firstname' => $firstname,
            'lastname' => $lastname,
            'pseudo' => $pseudo,
            'email' => $email,
            'password' => $password,
            'role_id' => $role
        ]);

        return $this->jsonResponse($user);
    }

    public function destroy($id){

        if (User::destroy($id)){
            $code = 204;
        }else{
            $code = 403;
        }

        return $this->jsonResponse(null, $code);
    }
}
