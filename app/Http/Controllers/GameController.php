<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Game;

class GameController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        $games = Game::with('teams')->get();

        return $this->jsonResponse($games);
    }

    public function show($id){
        $game = Game::with('teams')->find($id);

        return $this->jsonResponse($game);
    }

    public function create(Request $request){
        $name = $request->input('name');
        $description = $request->input('description');

        $game = Game::create([
            'name' => $name,
            'description' => $description
        ]);

        return $this->jsonResponse($game, 201);
    }

    public function update(Request $request, $id){
        $game = Game::find($id);

        $name = $request->input('name');
        $description = $request->input('description');

        $game->update([
            'name' => $name,
            'description' => $description
        ]);

        return $this->jsonResponse($game);
    }

    public function destroy($id){
        if (Game::destroy($id)){
            $code = 204;
        }else{
            $code = 403;
        }

        return $this->jsonResponse(null, $code);
    }
}
