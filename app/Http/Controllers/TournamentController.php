<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Tournament;

class TournamentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function all(){
        $tournaments = Tournament::with(['user','game'])->get();

        return $this->jsonResponse($tournaments);
    }

    public function index($game){
        $tournaments = Tournament::where('game_id', $game)->with('user')->get();

        return $this->jsonResponse($tournaments);
    }

    public function show($game, $id){
        $tournament = Tournament::where('game_id', $game)->with('user')->find($id);

        return $this->jsonResponse($tournament);
    }

    public function create($game, Request $request){
        $name = $request->input('name');
        $description = $request->input('description');
        $cashprice = $request->input('cashprice');
        $active = $request->input('active');
        $begintime = $request->input('begintime');
        $user_id = 1;

        $tournament = Tournament::create([
            'name' => $name,
            'description' => $description,
            'cashprice' => $cashprice,
            'begintime' => $begintime,
            'active' => $active,
            'user_id' => $user_id,
            'game_id' => $game
        ]);

        return $this->jsonResponse($tournament, 201);
    }

    public function update($game, Request $request, $id){
        $tournament = Tournament::where('game_id', $game)->with('user')->find($id);

        $name = $request->input('name');
        $description = $request->input('description');
        $cashprice = $request->input('cashprice');
        $active = $request->input('active');
        $begintime = $request->input('begintime');

        $tournament->update([
            'name' => $name,
            'description' => $description,
            'cashprice' => $cashprice,
            'begintime' => $begintime,
            'active' => $active
        ]);

        return $this->jsonResponse($tournament);
    }

    public function destroy($id){
        if (Tournament::destroy($id)){
            $code = 204;
        }else{
            $code = 403;
        }

        return $this->jsonResponse(null, $code);
    }
}
