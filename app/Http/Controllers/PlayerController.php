<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Player;

class PlayerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function all(){
        $tournaments = Player::with(['user', 'team'])->get();

        return $this->jsonResponse($tournaments);
    }

    public function index($team){
        $players = Player::where('team_id', $team)->with('user')->get();

        return $this->jsonResponse($players);
    }

    public function show($team, $id){
        $player = Player::where('team_id', $team)->with('user')->find($id);

        return $this->jsonResponse($player);
    }

    public function create($team){
        $user_id = 1;

        $player = Player::create([
            'user_id' => $user_id,
            'team_id' => $team
        ]);

        return $this->jsonResponse($player, 201);
    }

    public function destroy($id){
        if (Player::destroy($id)){
            $code = 204;
        }else{
            $code = 403;
        }

        return $this->jsonResponse(null, $code);
    }
}
