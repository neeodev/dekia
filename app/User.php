<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
      'firstname',
      'lastname',
      'pseudo',
      'email',
      'password',
      'token',
      'role_id'
    ];

    public function articles(){
      return $this->hasMany('App\Article');
    }

    public function comments(){
      return $this->hasMany('App\Comment');
    }

    public function tournaments(){
      return $this->hasMany('App\Tournament');
    }

    public function team(){
      return $this->hasOne('App\Team');
    }

    public function player(){
      return $this->hasOne('App\Player');
    }

    public function role(){
      return $this->belongsTo('App\Role');
    }
}