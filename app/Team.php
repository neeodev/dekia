<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
      'name',
      'user_id',
      'game_id'
    ];

    public function user(){
      return $this->belongsTo('App\User');
    }

    public function game(){
      return $this->belongsTo('App\Game');
    }

    public function players(){
      return $this->hasMany('App\Player');
    }
}