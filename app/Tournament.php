<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    protected $fillable = [
      'name',
      'description',
      'cashprice',
      'begintime',
      'active',
      'user_id',
      'game_id'
    ];

    public function game(){
      return $this->belongsTo('App\Game');
    }

    public function user(){
      return $this->belongsTo('App\User');
    }
}