<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


// Routing vers /api/v1 pour tout les données json
$router->group(['prefix' => '/api/v1'], function() use($router){

    
    $router->get('/players', 'PlayerController@all');
    $router->get('/teams', 'TeamController@all');
    $router->get('/tournaments', 'TournamentController@all');
    
    $router->group(['prefix' => 'users'], function() use($router){
        $router->get('/', 'UserController@index');
        $router->get('/{id}', 'UserController@show');
        $router->post('/', 'UserController@create');
        $router->put('/{id}', 'UserController@update');
        $router->delete('/{id}', 'UserController@destroy');
    });

    $router->group(['prefix' => 'games'], function() use($router){
        $router->get('/', 'GameController@index');
        $router->get('/{id}', 'GameController@show');
        $router->post('/', 'GameController@create');
        $router->put('/{id}', 'GameController@update');
        $router->delete('/{id}', 'GameController@destroy');

        
        $router->group(['prefix' => '{game}/tournaments'], function() use($router){
            $router->get('/', 'TournamentController@index');
            $router->get('/{id}', 'TournamentController@show');
            $router->post('/', 'TournamentController@create');
            $router->put('/{id}', 'TournamentController@update');
            $router->delete('/{id}', 'TournamentController@destroy');
        });

        
        $router->group(['prefix' => '{game}/teams'], function() use($router){
            $router->get('/', 'TeamController@index');
            $router->get('/{id}', 'TeamController@show');
            $router->post('/', 'TeamController@create');
            $router->put('/{id}', 'TeamController@update');
            $router->delete('/{id}', 'TeamController@destroy');

            $router->group(['prefix' => '{team}/players'], function() use($router){
                $router->get('/', 'PlayerController@index');
                $router->get('/{id}', 'PlayerController@show');
                $router->post('/', 'PlayerController@create');
                $router->delete('/{id}', 'PlayerController@destroy');
            });
        });
    });

    $router->group(['prefix' => 'roles'], function() use($router){
        $router->get('/', 'RoleController@index');
        $router->get('/{id}', 'RoleController@show');
        $router->post('/', 'RoleController@create');
        $router->put('/{id}', 'RoleController@update');
        $router->delete('/{id}', 'RoleController@destroy');
    });

    $router->group(['prefix' => 'articles'], function() use($router){
        $router->get('/', 'ArticleController@index');
        $router->get('/{slug}', 'ArticleController@show');
        $router->post('/', 'ArticleController@create');
        $router->put('/{id}', 'ArticleController@update');
        $router->delete('/{id}', 'ArticleController@destroy');

        $router->group(['prefix' => '{article}/comments'], function() use($router){
            $router->get('/', 'CommentController@index');
            $router->get('/{id}', 'CommentController@show');
            $router->post('/', 'CommentController@create');
            $router->put('/{id}', 'CommentController@update');
            $router->delete('/{id}', 'CommentController@destroy');
        });
    });

    $router->group(['prefix' => 'tags'], function() use($router){
        $router->get('/', 'TagController@index');
        $router->get('/{id}', 'TagController@show');
        $router->post('/', 'TagController@create');
        $router->put('/{id}', 'TagController@update');
        $router->delete('/{id}', 'TagController@destroy');
    });
});

// Redirige tout les url non utilisé vers VueJS
$router->get('/{router:.*}/', function () use ($router) {
    return view('app');
});
