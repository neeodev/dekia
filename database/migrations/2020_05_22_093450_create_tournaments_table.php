<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTournamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournaments', function (Blueprint $table) {
            $table->id();
            $table->string('name',50);
            $table->longText('description');
            $table->integer('cashprice');
            $table->datetime('begintime');
            $table->boolean('active');
            $table->foreignId('game_id');
            $table->foreignId('user_id');
            $table->timestamps();
            $table->unique(['name', 'begintime']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournaments');
    }
}
